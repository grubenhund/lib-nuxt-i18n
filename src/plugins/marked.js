import marked from 'marked';

export default function(ctx, inject) {
  inject('marked', marked);
}
